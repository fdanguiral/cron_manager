module.exports = (server) => {
    server.middlewares = {
        bodyParser: require('body-parser'),
        res: require("./res"),
        ensureBodyFields: require('./ensureBodyFields'),
        ensureAuthenticated: require('./ensureAuthenticated'),
        ensurePermissions: require('./ensurePermissions'),
        clean: require('./clean'),
}
};