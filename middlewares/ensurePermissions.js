const Acl = require("../models/Acl");
module.exports = (server, params) => {
    return (req, res, next) => {
        const acl = new Acl(server.settings.roles);
        if (params.role)
            if (!acl.is(req.auth.user, params.role))
                return res.status(403);
        if (params.permission)
            if (!acl.can(req.auth.user, params.permission))
                return res.status(403);

        next();
    }
};