module.exports = (req, res, next) => {
    res.error = (error) => {
        if (error.code)
            return res.status(error.code).send(error.reason);

        return res.status(500).send(error);
    };

    res.created = () => {
        res.status(201).send();
    };

    res.noContent = () => {
        res.status(204).send();
    };

    res.content = (data) => {
        res.status(200).send(data);
    };

    res.unauthorized = (data) => {
        res.status(401).send(data);
    };

    next();
};