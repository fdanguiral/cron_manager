class Acl {

    constructor(roles) {
        this.roles = roles;
    }

    can(user, permission) {
        // check if role exists
        let $role = this.roles[user.role];
        // check roles
        if ($role.can.indexOf(permission) !== -1)
            return true;
        //check inherits property
        if (!$role.inherits)
            return false;
        //check for inheritance
        while (true) {
            if ($role.inherits)
                $role = this.roles[$role.inherits];
            else
                break;
            if ($role.can.indexOf(permission) !== -1)
                return true;
        }
        return false;
    }

    is(user, role) {
        let $role = user.role;
        if ($role === role)
            return true;
        while (true) {
            if (this.roles[$role].inherits)
                $role = this.roles[$role].inherits;
            else
                break;
            if ($role === role)
                return true;
        }
        return false;
    }
}

module.exports = Acl;