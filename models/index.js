const mongoose = require('mongoose');
const Promise = require('bluebird');

module.exports = (server) => {
    server.mongoose = mongoose.connect(server.settings.db.url);
    server.mongoose.Promise = Promise;

    server.models = {
        User: require('./User'),
        Token: require('./Token'),
        Log: require('./Log'),
        Project: require('./Project')
    }
};