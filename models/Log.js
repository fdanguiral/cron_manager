const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamps');

const Schema = mongoose.Schema;

const LogSchema = Schema({
    message: {
        type:String,
        required: true
    },
    project:{
        type: Schema.Types.ObjectId,
        ref: 'Project',
    },
    showedBy: {
        type: Array
    },
    status: {
        type:String,
        required: true
    }
});

LogSchema.plugin(timestamps);
module.exports = mongoose.model('Log',LogSchema);