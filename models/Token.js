const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let TokenSchema = Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    createdAt: {
        type: Date,
        expires: 3600,
        default: Date.now
    }
});

module.exports = mongoose.model('Token', TokenSchema);
