const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamps');

const Schema = mongoose.Schema;

const UserSchema = Schema({
    username: {
        type: String,
        required: true,
    },
    password: {
        type:String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    enterprise: {
        type: String,
        required: true
    },
    role: {
        type: String,
        //required: true,
        default:'user'
    }

});
UserSchema.plugin(timestamps);
module.exports = mongoose.model('User',UserSchema);