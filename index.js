const express = require("express");
const server = express();
module.exports = server;
require('./settings')(server);              console.log('loading settings...');
require('./models')(server);                console.log('loading models...');
require('./middlewares')(server);           console.log('loading middlewares...');
require('./actions')(server);               console.log('loading actions...');
require('./routes')(server);                console.log('loading routes...');
const port = process.env.PORT || server.settings.port;

server.listen(port);
console.log(`app is running on port ${port}`);
