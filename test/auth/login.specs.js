const chai = require("chai");
const server = require("../../index");
const User = server.models.User;
const sha1 = require("sha1");
const expect = require("chai").expect;

describe("/POST auth login", function () {
    it("should login", (done) => {

        chai.request(server)
            .post("/auth/login")
            .send({
                "email": "user@gmail.com",
                "password": "user"
            })
            .end((err, res) => {
                res.should.to.have.status(200);
                done();
            });

    });
    it("should  fail", (done) => {

        chai.request(server)
            .post("/auth/login")
            .send({
                "email": "test@gmail.com",
                "password": "test123"
            })
            .end((err, res) => {
                res.should.to.have.status(422);
                done();
            });

    });

});
