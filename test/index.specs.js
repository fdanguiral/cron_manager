const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const db = require("./utils/db")();
chai.should();
beforeEach((done) => {
    describe("tearUp", ()=>{
        db.clear()
            .then(db.createDefaultUser)
            .then(db.createAdminUser)
            .then(()=>{done()});
    });
});

after(function(){
    console.log("tests finished");

});
