const mongoose = require("mongoose");
const Promise = require("bluebird");
const server = require("../../index");
const User = server.models.User;
const Project = server.models.Project;
const Log = server.models.Log;
const sha1 = require('sha1');
const chai = require('chai');


module.exports = () => {
    return {
        clear: clear,
        createDefaultUser: createDefaultUser,
        getToken: getToken,
        createAdminUser: createAdminUser,
        getAdminToken: getAdminToken,
        createDefaultProject: createDefaultProject,
        getDefaultUser: getDefaultUser,
        getAdminUser: getAdminUser,
        createDefaultLog: createDefaultLog
    }
};

function clear() {
    return new Promise((resolve, reject) => {

        mongoose.createConnection(server.settings.db.url, function () {
            mongoose.connection.db.dropDatabase((err, res) => {
                if (err)
                    reject("bdd not cleaned");
                console.log("bdd cleaned");
                resolve();
            });
        })
    });
}

function createAdminUser() {
    let user = new User({
        "email": "admin@gmail.com",
        "password": sha1("admin"),
        "username": "admin",
        "enterprise": "admin",
        "role": "admin"
    });
    console.log("insert admin user");
    return saveUser(user);
}
function createDefaultUser() {
    let user = new User({
        "email": "user@gmail.com",
        "password": sha1("user"),
        "username": "user",
        "enterprise": "user",
        "role": "user"
    });
    console.log("insert default user");
    return saveUser(user);
}
function saveUser(user) {
    return new Promise((resolve, reject) => {
        user.save()
            .then(() => {
                resolve();
            })
            .catch();
    });
}

function getToken() {
    return new Promise((resolve, reject) => {
        chai.request(server)
            .post("/auth/login")
            .send({
                "email": "user@gmail.com",
                "password": "user"
            })
            .end((err, res) => {
                if (err) {
                    //console.log(err);
                    reject();
                }
                resolve(res.text);

            });
    });
}
function getAdminToken() {
    return new Promise((resolve, reject) => {
        chai.request(server)
            .post("/auth/login")
            .send({
                "email": "admin@gmail.com",
                "password": "admin"
            })
            .end((err, res) => {
                if (err) {
                    //console.log(err);
                    reject();
                }
                resolve(res.text);

            });
    });
}
function createDefaultProject() {
    console.log("insert default project");
    return User.findOne({"email": "user@gmail.com"})
        .then(createProject);

    function createProject(user) {
        let project = new Project({
            "owner": user,
            "name": "test"
        });
        return project.save()
    }
}
function createDefaultLog() {
    console.log("insert default log");
    return Project.findOne({"name": "test"})
        .then(createLog);

    function createLog(project) {
        let log = new Log({
            "project": project._id,
            "message": "msg",
            "status": "success"
        });
        return log.save()
    }
}
function getDefaultUser() {
    return User.findOne({"email": "user@gmail.com"});
}

function getAdminUser() {
    return User.findOne({"email": "user@gmail.com"});
}