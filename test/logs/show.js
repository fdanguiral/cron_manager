const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/logs/:id /GET show', () => {
    it('should get log', (done) => {
        let project;
        let token;
        let log;
        db.createDefaultProject()
            .then((p) => {project = p})
            .then(db.createDefaultLog)
            .then((l) => {log = l})
            .then(db.getToken)
            .then((t) => {token = t})
            .then(doTest);

        function doTest() {
            chai.request(server)
                .get('/logs/' + log._id)
                .set('authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                })
        }
    });
});