const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/logs /POST', () => {
    it('should create a log', (done) => {
        let project;
        let user;
        let token;
        db.createDefaultProject()
            .then((p)=>{project=p})
            .then(db.getDefaultUser)
            .then((u)=>{user=u})
            .then(db.getToken)
            .then((t)=>{token=t})
            .then(doTest);

        function doTest() {
            chai.request(server)
                .post('/logs')
                .set('authorization', token)
                .send({
                    "message":"msg",
                    "status":"success",
                    "project": project._id
                })
                .end((err,res)=>{
                    res.should.have.status(201);
                    done();
                });

        }
    });
});