const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();
describe('/logs /GET list', () => {
    it('should get logs', (done) => {
        let project;
        let token;
        let log;
        db.createDefaultProject()
            .then((p) => {project = p})
            .then(db.createDefaultLog)
            .then((l) => {log = l})
            .then(db.getToken)
            .then((t) => {token = t})
            .then(getList);

        function getList() {
            chai.request(server)
                .get('/logs')
                .set('authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.length.should.be.eql(1);
                    done();
                })
        }
    });
});