const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/logs /DELETE', () => {
    it('should delete a log', (done) => {

        let log;
        let token;
        db.createDefaultProject()
            .then(db.createDefaultLog)
            .then((l)=>{log=l})
            .then(db.getToken)
            .then((t)=>{token=t})
            .then(doTest);

        function doTest() {
            chai.request(server)
                .delete('/logs/'+log._id)
                .set('authorization', token)
                .send()
                .end((err,res)=>{
                    res.should.have.status(204);
                    done();
                });

        }
    });
});