const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();
describe("/logs PUT", () => {
    it('should update', function (done) {
        let project;
        let token;
        let log;
        db.createDefaultProject()
            .then((p) => {project = p})
            .then(db.createDefaultLog)
            .then((l) => {log = l})
            .then(db.getToken)
            .then((t) => {token = t})
            .then(update);

        function update() {
            chai.request(server)
                .put('/logs/' + log._id)
                .set('authorization', token)
                .send({
                    "message": "updated",
                    "status":"updated"
                })
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        }
    });
});