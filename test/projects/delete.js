const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/projects/:id /DELETE ', () => {
    it('should remove project', (done) => {
        let project;
        db.createDefaultProject()
            .then((p) => {
                project = p
            })
            .then(db.getToken)
            .then(doTest);

        function doTest(token) {
            chai.request(server)
                .delete('/projects/' + project._id)
                .set('authorization', token)
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                })
        }
    });
});