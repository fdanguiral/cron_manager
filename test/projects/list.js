const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();
describe('/projects /GET list', () => {
    it('should get projects', (done) => {
        let project;
        db.createDefaultProject()
            .then((p) => {
                project = p
            })
            .then(db.getToken)
            .then(getList);

        function getList(token) {
            chai.request(server)
                .get('/projects')
                .set('authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.length.should.be.eql(1);
                    done();
                })
        }
    });
});