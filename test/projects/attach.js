const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/projects /POST attach', () => {
    it('should attach an user', (done) => {
        let project;
        let token;
        let user;
        db.createDefaultProject()
            .then((p) => {project = p})
            .then(db.getToken)
            .then((t) => {token = t})
            .then(db.getDefaultUser)
            .then((u) => {user = u})
            .then(doTest);

        function doTest() {
            chai.request(server)
                .post('/projects/' + project._id + "/attach/" + user._id)
                .set('authorization', token)
                .send()
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        }
    });
});