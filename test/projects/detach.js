const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/projects /POST detach', () => {
    it('should detach an user', (done) => {
        let project;
        let token;
        let user;
        db.createDefaultProject()
            .then((p) => {project = p})
            .then(db.getToken)
            .then((t) => {token = t})
            .then(db.getDefaultUser)
            .then((u) => {user = u})
            .then(attach)
            .then(doTest);
        function attach() {
            project.users.push(user);
            return project.save();
        }
        function doTest() {
            chai.request(server)
                .post('/projects/' + project._id + "/detach/" + user._id)
                .set('authorization', token)
                .send()
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        }
    });
});