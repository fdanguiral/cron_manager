const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();
describe("/project PUT", () => {
    it('should update', function (done) {
        let project;
        db.createDefaultProject()
            .then((p) => {
                project = p;
            })
            .then(db.getToken)
            .then(update);

        function update(token) {
            chai.request(server)
                .put('/projects/' + project._id)
                .set('authorization', token)
                .send({
                    "name": "updated"
                })
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        }
    });
});