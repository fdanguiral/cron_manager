const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();

describe('/projects/:id /GET show', () => {
    it('should get project', (done) => {
        let project;
        db.createDefaultProject()
            .then((p) => {
                project = p
            })
            .then(db.getToken)
            .then(doTest);

        function doTest(token) {
            chai.request(server)
                .get('/projects/' + project._id)
                .set('authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                })
        }
    });
});