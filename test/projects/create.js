const chai = require('chai');
const server = require('../../index');
const db = require('../utils/db')();
const should = chai.Should();
describe("projects GET", () => {
    it("should create a project and have a status 201", (done) => {
        db.getToken()
            .then(doTest);

        function doTest(token) {
            chai.request(server)
                .post('/projects')
                .set('authorization',token)
                .send({
                    "name": "cdc"
                })
                .end((err, res)=> {
                    res.should.have.status(201);
                    done();
                })
        }
    });
});