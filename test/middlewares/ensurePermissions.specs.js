const chai = require("chai");
const server = require("../../index");
const expect = require("chai").expect;
const httpMocks = require("node-mocks-http");
const ensurePermissions = require("../../middlewares/ensurePermissions");
describe("test ensurePermissions middleware", () => {
    it("should be admin an authorized", (done) => {
        params = {role: "admin"};
        const req = {auth: {user: {role: "admin"}}};
        let res = httpMocks.createResponse();
        let mw = ensurePermissions(server, params);
        let result = false;
        mw(req, res, () => {
            result = true;
        });
        expect(true).to.eql(result);
        done();
    })
    it("should be admin but is guest", (done) => {
        let params = {role: "admin"};
        let req = {auth: {user: {role: "guest"}}};
        let res = httpMocks.createResponse();
        let mw = ensurePermissions(server, params);
        let result = false;
        mw(req, res, () => {
            result = true;
        });
        expect(false).to.eql(result);
        done();
    })
    it("should can write", (done) => {
        let params = {can: "write"};
        let req = {auth: {user: {role: "admin"}}};
        let res = httpMocks.createResponse();
        let mw = ensurePermissions(server, params);
        let result = false;
        mw(req, res, () => {
            result = true;
        });
        expect(true).to.eql(result);
        done();
    });
    it("should guest can't write", (done) => {
        let params = {permission: "write"};
        let req = {auth: {user: {role: "guest"}}};
        let res = httpMocks.createResponse();
        let mw = ensurePermissions(server, params);
        let result = false;
        mw(req, res, () => {
            result = true;
        });
        expect(false).to.eql(result);
        done();
    });
});
