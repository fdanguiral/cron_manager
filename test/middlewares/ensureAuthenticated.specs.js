const chai = require("chai");
const sinon = require("sinon");
const server = require("../../index");
const db = require("../utils/db")();
const httpMocks = require("node-mocks-http");
const ensureAuthenticated = require("../../middlewares/ensureAuthenticated");
const expect = require("chai").expect;
const Promise = require("bluebird");
const assert = require("assert");

let mw = ensureAuthenticated(server);

describe("Middleware ensureAuthenticated", () => {


    it("Should return a function", (done) => {
        mw.should.to.be.a('function');
        done();
    });

    it('should accept three arguments', (done) => {
        expect(mw.length).to.equal(3);
        done();
    });

    it('should authenticate with success', (done) => {
        db.getToken()
            .then((token) => {

                let res = httpMocks.createResponse();
                let req = httpMocks.createRequest({headers: {authorization: token}});
                let result = false;

                mw(req, res, () => {
                    result = true;
                });
                Promise.delay(2000).then(() => {
                    expect(true).to.eql(result);
                    done();
                });
            });
    });
    it('should authenticate with failure', (done) => {
        db.getToken()
            .then((token) => {

                let res = httpMocks.createResponse();
                let req = httpMocks.createRequest({headers: {authorization: "aaa"}});
                let result = false;

                mw(req, res, () => {
                    result = true;
                });
                Promise.delay(2000).then(() => {
                    expect(false).to.eql(result);
                    done();
                });
            });
    });
});