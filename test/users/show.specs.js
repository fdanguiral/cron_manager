const chai = require('chai');
const server = require('../../index');
const User = server.models.User;
const expect = chai.expect;
const db = require('../utils/db')();
describe("/GET user", () => {

    it("Should show all users", (done) => {
        let user = new User({
            "email": "test2@gmail.com",
            "password": "test2",
            "username": "test2",
            "enterprise": "test2"
        });
        db.getToken().then((token) => {
            user.save((err, user) => {
                chai.request(server)
                    .get('/users/' + user._id)
                    .set('authorization', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        done();
                    });
            });
        });
    });
});
