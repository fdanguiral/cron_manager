const chai = require('chai');
const server = require('./../../index');
const User = server.models.User;
const db = require('../utils/db')();

describe('/DELETE users', () => {
    it("Should delete an user", (done) => {
        let user = new User({
            "email": "test@gmail.com",
            "password": "test",
            "username": "test",
            "enterprise": "test"
        });
        db.getAdminToken()
            .then((token) => {
                user.save((err, user) => {
                    chai.request(server)
                        .delete('/users/' + user._id)
                        .set('authorization', token)
                        .end((err, res) => {
                            res.should.have.status(204);
                            done();
                        });
                });
            });
    });
});
