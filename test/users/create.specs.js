const server = require("../../index");
const db = require('../utils/db')();
const chai = require('chai');

describe("/POST create", () => {
    it("Should create an user", (done) => {
        chai.request(server)
            .post("/users")
            .send({
                "email": "test2@gmail.com",
                "password": "test2",
                "username": "test2",
                "enterprise": "test2"
            })
            .end((err, res) => {
                res.should.have.status(201);
                done();
            });
    });

    it("Should failed and have status 400 if password or email is missing", (done) => {
        chai.request(server)
            .post("/users")
            .send({
                "password": "test2",
                "username": "test2",
                "enterprise": "test2"
            })
            .end((err, res) => {
                res.should.have.status(400);
                done();
            });
    });
});



