const chai = require("chai");
const server = require("../../index");
const User = server.models.User;
const db = require('../utils/db')();
describe('/PUT users', () => {
    it('should update user and have a status 204', (done) => {
        let user = new User({
            "email": "test2@gmail.com",
            "password": "test2",
            "username": "test2",
            "enterprise": "test2"
        });
        db.getAdminToken().then((token) => {
            user.save((err, user) => {
                chai.request(server)
                    .put('/users/' + user._id)
                    .set('authorization', token)
                    .send({"username": "test2update"})
                    .end((err, res) => {
                        res.should.have.status(204);
                        res.body.should.be.a("object");
                        done();
                    });
            });
        });
    });
});
