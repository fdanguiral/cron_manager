const server = require("../../index");
const db = require('../utils/db')();
const chai = require('chai');

describe('/GET list', () => {
    it("Should have status 200", (done) => {
        db.getToken()
            .then((token) => {
                chai.request(server)
                    .get('/users')
                    .set('authorization',token)
                    .end(function (err, res) {
                        res.should.to.have.status(200);
                        res.body.length.should.be.eql(2);
                        done();
                    });
            })
            .catch((err)=>{console.log("erreur")});
    });
});







