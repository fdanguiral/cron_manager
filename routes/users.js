const express = require('express');
const router = express.Router();
module.exports = (server) => {

    router.get('/:id', server.actions.users.show);
    router.get('/',
        server.middlewares.ensureAuthenticated(server),
        server.middlewares.ensurePermissions(server,{"role":"guest"}),
        server.actions.users.list);

    router.post('/',
        server.middlewares.bodyParser.json(),
        server.middlewares.ensureBodyFields(server.models.User.schema),
        server.actions.users.create);

    router.put('/:id',
        server.middlewares.ensureAuthenticated(server),
        server.middlewares.bodyParser.json(),
        server.middlewares.clean(['password', '_id']),
        server.actions.users.update);
    router.delete('/:id',
        server.middlewares.ensureAuthenticated(server),
        server.actions.users.remove);

    return router;
};