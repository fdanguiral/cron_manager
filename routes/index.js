
module.exports = (server) => {
    server.use(server.middlewares.res);
    server.use('/users', require('./users')(server));
    server.use('/auth', require('./auth')(server));
    server.use('/logs',require('./logs')(server));
    server.use('/projects', require('./projects')(server));
};