module.exports = (server) => {
    const router = require('express').Router();

    router.post('/',
        server.middlewares.bodyParser.json(),
        server.middlewares.ensureAuthenticated(server),
        server.actions.logs.create
    );
    router.put('/:id',
        server.middlewares.bodyParser.json(),
        server.middlewares.ensureAuthenticated(server),
        server.middlewares.clean(['_id']),
        server.actions.logs.update
    );
    router.delete('/:id',
        server.middlewares.ensureAuthenticated(server),
        server.actions.logs.remove
    );
    router.get('/',
        server.middlewares.ensureAuthenticated(server),
        server.actions.logs.list
    );
    router.get('/:id',
        server.middlewares.ensureAuthenticated(server),
        server.actions.logs.show
    );

    return router;
};