module.exports = (server) => {
    const router = require("express").Router();

    router.post('/login',
        server.middlewares.bodyParser.json(),
        server.middlewares.ensureBodyFields(['email', 'password']),
        server.actions.auth.login
    );

    return router;
};