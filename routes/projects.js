module.exports = (server) => {
    const router = require('express').Router();

    router.post('/',
        server.middlewares.bodyParser.json(),
        server.middlewares.ensureAuthenticated(server),
        server.actions.projects.create
    );
    router.put('/:id',
        server.middlewares.bodyParser.json(),
        server.middlewares.ensureAuthenticated(server),
        server.middlewares.clean(['_id']),
        server.actions.projects.update
    );
    router.delete('/:id',
        server.middlewares.ensureAuthenticated(server),
        server.actions.projects.remove
    );
    router.get('/',
        server.middlewares.ensureAuthenticated(server),
        server.actions.projects.list
    );
    router.get('/:id',
        server.middlewares.ensureAuthenticated(server),
        server.actions.projects.show
    );
    router.post('/:id/attach/:userId',
        server.middlewares.ensureAuthenticated(server),
        server.actions.projects.attach
    );
    router.post('/:id/detach/:userId',
        server.middlewares.ensureAuthenticated(server),
        server.actions.projects.detach
    );

    return router;
};