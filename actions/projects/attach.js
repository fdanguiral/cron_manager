module.exports = (server) => {
    const Project = server.models.Project;
    const User = server.models.User;
    return (req,res) => {

        User.findById(req.params.userId)
            .then(ensureOne)
            .then(getProject)
            .then(ensureOne)
            .then(notAlreadyInUsers)
            .then(attach)
            .then(res.noContent)
            .catch(res.error);

        function ensureOne(data) {
            return (data) ? data : Promise.reject({code: 404});
        }
        function getProject() {
            return Project.findById(req.params.id);
        }
        function notAlreadyInUsers(project) {
            return project.users.indexOf(req.params.userId) !== -1 ?
                Promise.reject({code: 404}) : project;
        }
        function attach(project) {
            project.users.push(req.params.userId);
            project.save();
        }
    }
};