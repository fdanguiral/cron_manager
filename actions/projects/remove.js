module.exports = (server) => {
    const Acl = require('../../models/Acl');
    return (req,res) => {
        const Project = server.models.Project;
        const acl = new Acl(server.settings.roles);

        Project.findById(req.params.id)
            .then(ensureOne)
            .then(ensureOwnerOrAdmin)
            .then(removeLog)
            .then(res.noContent)
            .catch(res.error);
        function ensureOne(data) {
            return (data) ? data : Promise.reject({code: 404});
        }
        function ensureOwnerOrAdmin(project) {
            return project.owner !== req.auth.userId && !acl.is(req.auth.user, 'admin')
                ? project : Promise.reject({code: 404});
        }
        function removeLog() {
            return Project.remove({"_id": req.params.id})
        }
    }
};