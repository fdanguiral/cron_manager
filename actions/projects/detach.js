const Promise = require('bluebird');
module.exports = (server) => {
    const Project = server.models.Project;
    const User = server.models.User;
    return (req, res) => {
        User.find(req.params.userId)
            .then(ensureOne)
            .then(getProject)
            .then(ensureOne)
            .then(canDetach)
            .then(detach)
            .then(res.noContent)
            .catch(res.error);

        function ensureOne(data) {
            return data ? data : Promise.reject({code: 404});
        }

        function getProject() {
            return Project.findById(req.params.id);
        }

        function canDetach(project) {
            return project.users.indexOf(req.params.userId) !== -1 ?
                project : Promise.reject({code: 404});
        }

        function detach(project) {
            project.users.forEach((userId, index) => {
                if(userId == req.params.userId)
                    project.users.splice(index,1);
            })

            return project.save();
        }
    }
};