const Promise = require('bluebird');
module.exports = (server) => {
    const Project = server.models.Project;
    return (req, res) => {
        Project.findById(req.params.id)
            .then(ensureOne)
            .then(ownerOrAdmin)
            .then(update)
            .then(res.noContent)
            .catch(res.error);

        function ensureOne(data) {
            return data ? data : Promise.reject({'code': 404});
        }

        function ownerOrAdmin(project) {
            return (req.auth.userId != project.owner && req.auth.user.role !== 'admin') ?
                Promise.reject({'code': 404}) : project
        }
        function update(project) {
            Object.keys(req.body).forEach((key) => {
                project[key] = req.body[key]
            });
            project.save();
            return project;
        }

    }
};