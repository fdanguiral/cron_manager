module.exports = (server) => {
    return (req,res) => {
        const Project = server.models.Project;
        let project = new Project(req.body);
        project.owner = req.auth.userId;
        project.save()
            .then(res.created)
            .catch(res.error);
    }
};