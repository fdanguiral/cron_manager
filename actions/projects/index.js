module.exports = (server) => {
    return {
        create: require('./create')(server),
        list: require('./list')(server),
        show: require('./show')(server),
        update: require('./update')(server),
        remove: require('./remove')(server),
        attach: require('./attach')(server),
        detach: require('./detach')(server)
    }
};