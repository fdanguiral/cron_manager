const jwt = require('jsonwebtoken');
const sha1 = require('sha1');
const Promise = require('bluebird');
module.exports = (server) => {
    const User = server.models.User;
    const Token = server.models.Token;

    return (req, res, next) => {
        let email = req.body.email;
        let password = sha1(req.body.password);
        User.findOne({email: email, password: password})
            .then(ensureOne)
            .then(generateToken)
            .catch(res.error);

        function ensureOne(data) {
            return data ? data : Promise.reject({"code":422});
        }
        function generateToken(user) {
            new Token({userId: user._id}).save((err, token) => {
                if (err)
                    return Promise.reject({"code":422});

                let accessToken = jwt.sign({accessToken: token._id}, server.settings.TOKEN_SECRET); // encrypt the access token created from the database.

                return res.status(200).send(accessToken);
            });
        }

    };
};