module.exports = (server) => {
    return (req,res) => {
        const Log = server.models.Log;
        Log.findById(req.params.id)
            .then(ensureOne)
            .then(res.content)
            .catch(res.error);

        function ensureOne(data) {
            return (data) ? data : Promise.reject({code: 404});
        }
    }
};