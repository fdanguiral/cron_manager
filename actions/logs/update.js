module.exports = (server) => {
    const Log = server.models.Log;
    return (req,res) => {
        Log.findById(req.params.id)
            .populate('project')
            .then(ensureOne)
            .then(ownerOrAdmin)
            .then(update)
            .then(res.noContent)
            .catch(res.error);
        function ensureOne(data) {
            return data ? data : Promise.reject({'code': 404});
        }

        function ownerOrAdmin(log) {
            return (req.auth.userId != log.project.owner && req.auth.user.role !== 'admin') ?
                Promise.reject({'code': 404}) : log
        }
        function update(log) {
            Object.keys(req.body).forEach((key) => {
                log[key] = req.body[key]
            });
            log.save();
            return log;
        }
    }
};