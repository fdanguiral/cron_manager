module.exports = (server) => {
    const Acl = require('../../models/Acl');
    return (req,res) => {
        const Log = server.models.Log;
        const acl = new Acl(server.settings.roles);

        Log.findById(req.params.id)
            .then(ensureOne)
            .then(ensureOwnerOrAdmin)
            .then(removeLog)
            .then(res.noContent)
            .catch(res.error);
        function ensureOne(data) {
            return (data) ? data : Promise.reject({code: 404});
        }
        function ensureOwnerOrAdmin(log) {
            return log.owner !== req.auth.userId && !acl.is(req.auth.user, 'admin')
                ? log : Promise.reject({code: 404});
        }
        function removeLog() {
            return Log.remove({"_id": req.params.id})
        }
    }
};