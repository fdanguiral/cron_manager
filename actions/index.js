module.exports = (server) => {
    server.actions = {
        users: require("./users")(server),
        auth: require("./auth")(server),
        logs: require("./logs")(server),
        projects: require("./projects")(server),
    }
};