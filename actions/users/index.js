module.exports = (server) => {
    return {
        show: require("./show")(server),
        create: require("./create")(server),
        list: require("./list")(server),
        update: require("./update")(server),
        remove:require("./remove")(server)
    }
};