module.exports = (server) => {
    return (req, res, next) => {
        const User = server.models.User;
        User.findById(req.params.id)
            .then(ensureOne)
            .then(res.content)
            .catch(res.error);

        function ensureOne(data) {
            return (data) ? data : Promise.reject({code: 404});
        }
    }
};