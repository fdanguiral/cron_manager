const Acl = require('../../models/Acl');
module.exports = (server) => {
    const acl = new Acl(server.settings.roles);
    return (req,res) => {
        const User = server.models.User;

        if(req.params.id !== req.auth.userId && !acl.is(req.auth.user, 'admin'))
            return res.unauthorized();

        User.findOneAndUpdate(req.params.id, req.body)
            .then(res.noContent)
            .catch(res.error);

    }
};