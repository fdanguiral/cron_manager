const Acl = require('../../models/Acl');
module.exports = (server) => {
    const acl = new Acl(server.settings.roles);
    return (req, res) => {
        const User = server.models.User;

        if(!acl.is(req.auth.user, 'admin'))
            return res.unauthorized();

        User.remove({"_id": req.params.id})
            .then(res.noContent)
            .catch(res.error);
    }
};